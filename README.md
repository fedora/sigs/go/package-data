<!--
Copyright (C) 2024 Maxwell G <maxwell@gtmx.me>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# Fedora Go Package Data

This repository was initially created for managing the
[Mass_Retire_Golang_Leaves] Change,
but it now stores general data about the Fedora Go SIG's papckages.

[Mass_Retire_Golang_Leaves]: https://fedoraproject.org/wiki/Changes/Mass_Retire_Golang_Leaves

## Refresh data

The data in this repository is refreshed automatically on a nightly basis.

You can refresh data locally using `tox -e regen`.
Go SIG members can manually trigger a data refresh through Gitlab CI using the
[Run pipeline] feature.

[Run pipeline]: https://gitlab.com/fedora/sigs/go/package-data/-/pipelines/new

## Linters

This project enforces certain code style rules. Run `tox` before contributing
changes to ensure your code passes.

## Opting out

If your package is in the
[leaves list](https://pagure.io/GoSIG/go-leaves/blob/main/f/leaves)
([by maintainer]), you may opt out of the automatic retirement.
For instance, one of the "leaves" may be a dependency for a new piece of
software you're packaging.

To opt out, simply create a text file `opt-out/{component}`
and commit it to this repository.
Please include a brief justification.
Those without commit access may submit a PR.

``` bash
echo "Dependency for foo (review bug #XXXXX)" > ./opt-out/bar
git add ./opt-out/bar
git commit -m "opt out bar"
git push origin
```

## License

- The license data is licensed under the [UNLICENSE](./LICENSES/Unlicense.txt).
- Other files (notably, the scripts in the tools/ directory) have explicit
  licenses.

``` text
SPDX-License-Identifier: Unlicense AND GPL-2.0-or-later
```

[by maintainer]: https://pagure.io/GoSIG/go-leaves/blob/main/f/leaves-by-maintainer
