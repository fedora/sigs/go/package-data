#!/usr/bin/env python3

# Copyright (c) 2023 Maxwell G <maxwell@gtmx.me>
# SPDX-License-Identifier: GPL-2.0-or-later

import sys
from functools import cache
from typing import cast

import fedrq.config
import libdnf5
import typer

# We use the libdnf5 backend explicitly
from fedrq.backends.libdnf5.backend import Package, PackageQuery, Repoquery
from typing_extensions import Annotated

APP = typer.Typer(context_settings={"help_option_names": ["-h", "--help"]})


def err(*args) -> None:
    print("ERROR:", *args, file=sys.stderr)


@cache
def get_rq(branch: str, repo: str) -> Repoquery:
    rq = fedrq.config.get_config(backend="libdnf5").get_rq(branch, repo)
    return cast(Repoquery, rq)


def add_srpm_deps(rq: Repoquery, goal: libdnf5.base.Goal, package: Package) -> None:
    # Add the SRPMs requires to the goal
    for reldep in package.requires:
        goal.add_rpm_install(str(reldep))
    subpackages = rq.get_subpackages(rq.query(pkg=[package]))
    goal.add_rpm_install(subpackages)


def resolve_source_deps(
    rq: Repoquery, orphan_rpms: PackageQuery, binary_srpm: Package
) -> bool | None:
    goal = libdnf5.base.Goal(rq.base)
    add_srpm_deps(rq, goal, binary_srpm)
    tsx = goal.resolve()
    if tsx.get_problems():
        err("Failed to resolve:", binary_srpm)
        return None
    tsx_packages = tsx.get_transaction_packages()
    for package in tsx_packages:
        rpm_package = package.get_package()
        if package.get_action() == libdnf5.base.GoalAction_REMOVE:
            print(
                "During processing of", binary_srpm, "attempted to remove", rpm_package
            )
            return None
        if orphan_rpms.contains(rpm_package):
            return True
    return False


@APP.command()
def get_affected_orphans(
    orphans_file: typer.FileText = typer.Option(
        "outliers-orphan",
        "-O",
        "--orphans-file",
        encoding="utf-8",
        help="A file containing a list of orphaned source packages",
    ),
    binaries_file: typer.FileText = typer.Option(
        "binaries",
        "-B",
        "--binaries-file",
        help="A file containing a list of source packages that we want to keep.",
        encoding="utf-8",
    ),
    invert: Annotated[
        bool, typer.Option(help="Print the packages that are not affected")
    ] = False,
    branch: str = "rawhide",
    repo: str = "@base",
):
    """
    Print the source packages that are affected by orphaned libraries.
    """

    orphans = [line.strip() for line in orphans_file]
    orphans_file.close()
    binaries = [line.strip() for line in binaries_file]
    binaries_file.close()

    rq = get_rq(branch, repo)
    binary_query = rq.query(name=binaries, arch="src")
    orphan_source_query = rq.query(name=orphans, arch="src")
    orphan_subpkg_query = rq.get_subpackages(orphan_source_query)
    for package in binary_query:
        result = resolve_source_deps(
            rq, cast(PackageQuery, orphan_subpkg_query), package
        )
        if invert:
            if result is False:
                print(package.name)
        elif result:
            print(package.name)


if __name__ == "__main__":
    APP()
