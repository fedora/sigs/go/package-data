#!/usr/bin/env python3

# Copyright (c) 2023 Maxwell G <maxwell@gtmx.me>
# SPDX-License-Identifier: GPL-2.0-or-later

from __future__ import annotations

import argparse
import sys
from pathlib import Path
from typing import TYPE_CHECKING

import fedrq.cli.formatters
import fedrq.config

if TYPE_CHECKING:
    from fedrq.backends.base import PackageCompat, PackageQueryCompat, RepoqueryBase


def _directory(val: str) -> Path:
    path = Path(val)
    if path.exists() and not path.is_dir():
        raise ValueError(f"{path} must be a directory!")
    path.mkdir(exist_ok=True)
    return path


def parseargs(argv: list[str] | None = None) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="subcommand", required=True)
    subparsers_dict: dict[str, argparse.ArgumentParser] = {}
    ft = argparse.FileType("w")
    defaultft = ft("-")

    o_parent = argparse.ArgumentParser(add_help=False)
    o_parent.add_argument("-o", "--output", type=ft, default=defaultft)

    f_parent = argparse.ArgumentParser(add_help=False)
    get_formatter = fedrq.cli.formatters.DefaultFormatters.get_formatter
    f_parent.add_argument(
        "-F", "--formatter", type=get_formatter, default=get_formatter("name")
    )
    f_parent.add_argument("-I", "--ignore", nargs="*")
    f_parent.add_argument(
        "-D",
        "--dependencies",
        nargs="*",
        # help="Check for the dependents of these packages."
        # " By default, the dependents of golang are checked.",
        #
        # Provisional
        help=argparse.SUPPRESS,
    )
    f_parent.add_argument(
        "--error-for-invalid-ignores",
        action=argparse.BooleanOptionalAction,
        default=True,
    )

    for sub in ("leaves", "binaries", "outliers"):
        subparsers_dict[sub] = subparsers.add_parser(sub, parents=[o_parent, f_parent])
    subparsers_dict["all"] = subparsers.add_parser("all", parents=[f_parent])
    subparsers_dict["all"].add_argument(
        "-o", "--output", type=_directory, default=Path(".")
    )
    parsed = parser.parse_args(argv)
    return parsed


def get_go_subpackages(rq: RepoqueryBase) -> PackageQueryCompat:
    """
    Return query of all of golang.src's subpackages and go-rpm-macros
    """
    query = rq.query(name="go-rpm-macros", arch="notsrc")
    golang_pkgs = rq.get_subpackages([rq.get_package("golang", arch="src")])
    query = query.union(golang_pkgs)
    return query


def get_packages(
    rq: RepoqueryBase, ignore_list: list[str], dependencies: list[str]
) -> tuple[dict[str, list[PackageCompat]], list[str]]:
    matched_ignores = set()
    if dependencies:
        query = rq.get_subpackages(rq.query(name=dependencies, arch="src"))
    else:
        query = get_go_subpackages(rq)
    whatrequires = rq.query(
        requires=query,
        arch="src",
        # golang depends on itself
        name__neq="golang",
        latest=1,
    )
    results: dict[str, list[PackageCompat]] = {
        "binaries": [],
        "leaves": [],
        "outliers": [],
        "all_packages": [],
    }
    for srpm in whatrequires:
        subpkgs = rq.get_subpackages([srpm])
        arches = {subpkg.arch for subpkg in subpkgs}
        results["all_packages"].append(srpm)
        if srpm.name in ignore_list:
            matched_ignores.add(srpm.name)
        # If SRPM contains any arched subpackages
        if rq.base_arches & arches:
            results["binaries"].append(srpm)
        elif srpm.name in ignore_list:
            pass
        # If SRPM only contains noarch *-devel subpackages with zero dependents
        elif (
            arches == {"noarch"}
            and not rq.query(requires=subpkgs)
            and all("-devel" in p.name for p in subpkgs)
        ):
            results["leaves"].append(srpm)
        else:
            # Library only packages
            results["outliers"].append(srpm)
    if remainder := set(ignore_list) - matched_ignores:
        invalid_ignores = list(remainder)
    else:
        invalid_ignores = []

    return results, invalid_ignores


def main() -> int:
    args = parseargs()
    rq = fedrq.config.get_config().get_rq("rawhide", "buildroot")
    results, invalid_ignores = get_packages(
        rq, ignore_list=args.ignore or [], dependencies=args.dependencies or []
    )
    if args.subcommand == "all":
        for key in results:
            out = args.output / key
            with out.open("w", encoding="utf-8") as fp:
                for line in args.formatter.format(results[key]):
                    fp.write(line + "\n")
    else:
        for line in args.formatter.format(results[args.subcommand]):
            args.output.write(line + "\n")
    if invalid_ignores:
        fmti = "\n".join(invalid_ignores)
        out = f"""\


Invalid ignores found:
----------------------
{fmti}
"""
        sys.stderr.write(out)
        return int(args.error_for_invalid_ignores)
    return 0


if __name__ == "__main__":
    sys.exit(main())
