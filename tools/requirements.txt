# Copyright (c) 2024 Maxwell G <maxwell@gtmx.me>
# SPDX-License-Identifier: GPL-2.0-or-later

fedrq
typer
typing_extensions
